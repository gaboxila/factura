﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class modeloCliente
    {
        private static List<Cliente> listaClientes = new List<Cliente>();

        public static List<Cliente> getListaClientes()
        {
            return listaClientes;
        }

        public static void llenar()
        {
            Cliente[] clientes =
            {
                new Cliente(1, "001-235434-0076P", "Gabriel", "Bonilla", "22453345", "gabriel@gmail.com", "DDF nicalit 400 metros al sur"),
                new Cliente(2, "002-345222-3884S", "Jose", "Chamorro", "45342343", "jose@gmail.com", "DDF julio martinez 300mts")
            };

            foreach(Cliente cl in clientes)
            {
                listaClientes.Add(cl);
            }
        }
    }
}
