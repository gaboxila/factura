﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionClientes : Form
    {
        DataSet dsClientes;
        BindingSource bsClientes;

        public FrmGestionClientes()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataSet DsClientes
        {
            set
            {
                dsClientes = value;
            }
        }

        private void FrmGestionClientes_Load(object sender, EventArgs e)
        {
            bsClientes.DataMember = dsClientes.Tables["Clientes"].TableName;
            bsClientes.DataSource = dsClientes;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.Show();
        }
    }
}
