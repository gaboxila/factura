﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio, p.Sku + " -- " + p.Nombre );
            } 

            EmpleadoModel.Populate();
            foreach(Empleado emp in EmpleadoModel.GetLstEmpleado())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id,emp.Inss,
                    emp.Cedula,emp.Nombres,emp.Apellidos,emp.Direccion,
                    emp.Tconvencional,emp.Tcelular,emp.Sexo,emp.Salario,emp.Nombres + " " + emp.Apellidos);
            }

            modeloCliente.llenar();
            foreach(Cliente cl in modeloCliente.getListaClientes())
            {
                dsProductos.Tables["Clientes"].Rows.Add(cl.Id, cl.Cedula, cl.Nombre, cl.Apellido, cl.Telefono, cl.Correo,
                    cl.Direccion);
            }
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void crearFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsEmpleados = dsProductos;
            ff.Show();
        }

        private void gestionFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsSistema = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }
    }
}
